'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class items extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  items.init({
    id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    nama: DataTypes.STRING,
    stock: DataTypes.INTEGER,
    description: DataTypes.STRING,
    price: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'items',
    underscored: true,
  });
  return items;
};