'use strict';

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.users, {as:'user', foreignKey:'user_id'})
      this.belongsToMany(models.items,{through: models.orderdetails, as:'order', foreignKey:'orderID'})
    }
  }
  orders.init({
    id: {type: sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    userid: DataTypes.INTEGER,
    total: DataTypes.INTEGER,
    status: {type: sequelize.STRING, defaultValue: "belum bayar"}
  }, {
    sequelize,
    modelName: 'orders',
    underscored: true,
  });
  return orders;
};
