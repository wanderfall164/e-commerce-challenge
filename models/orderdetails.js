'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orderdetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
    }
  }
  orderdetails.init({
    orderID: DataTypes.INTEGER,
    itemID: DataTypes.INTEGER,
    jumlah: DataTypes.INTEGER,
    harga: DataTypes.INTEGER,
    subtotal: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'orderdetails',
    underscored: true,
  });
  return orderdetails;
};