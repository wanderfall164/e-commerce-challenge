'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('orderdetails', {
      orderID: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        references: {model:'orders', key:'id'}
      },
      itemID: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        references: {model:'items', key:'id'}
      },
      jumlah: {
        type: Sequelize.INTEGER
      },
      harga: {
        type: Sequelize.INTEGER
      },
      subtotal: {
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('orderdetails');
  }
};