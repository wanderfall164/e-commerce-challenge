require('dotenv').config();

const {DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS} = process.env;

module.exports = {
  development: {
    username: 'postgres',
    password: 'H1kazum!',
    database: 'E-commerce',
    host: 'localhost',
    port: 5432,
    dialect: "postgres",
  },
  test: {
    username: 'postgres',
    password: 'H1kazum!',
    database: 'E-commerce',
    host: 'localhost',
    port: 5432,
    dialect: "postgres",
  },
  production: {
    username: 'postgres',
    password: 'H1kazum!',
    database: 'E-commerce',
    host: 'localhost',
    port: 5432,
    dialect: "postgres",
    logging: false,
  },
};
