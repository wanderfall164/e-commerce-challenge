const db = require("../models");
const Orderdetails = db.sequelize.model('orderdetails')
const User = db.sequelize.model('users');
const Order = db.sequelize.model('orders')
const jwt = require('jsonwebtoken')
const cekToken = async (req, res, next) => {
    let authHeader = req.headers[authorization];
    const token = authHeader.split(' ')[1];
    try {
        const email = jwt.verify(token, "secretuser");
        const getUserID = await User.findOne({where:{
            email: email
    }})
    req.user = getUserID.id}
    catch(error){
        console.log(error.message)
        return res.status(401).send('Unauthorized user, access forbidden')
    }
    next()}
const register = async (req, res) => {
    const {email, name, password} = req.body;

    try {
        const existUser = await User.findOne({
            where: { email:email }
        });
        if(existUser) {
            return res.status(400).send("Gagal mendaftar, email sudah pernah digunakan")

        }
        const userCreated = await User.create({email:email, name:name, password:password})
    
        if(userCreated) {
            const authentication = jwt.sign(email,"secretuser")
            return res.cookie(authentication).status(201).send("User berhasil ditambahkan. UserID: " + userCreated.id)
        }
        
    } catch (error) {
        console.log(error.message)
        return res.status(500).send("Gagal menambahkan user")
    }
}
const login = async (req, res) => {
    const {email, password} = req.body;
    try {
        const loginUser = await User.findOne({
            where: {email: email, password:password}
        })
        if (loginUser === null){
            return res.status(400).send("Maaf, password atau email salah")
        }
        const authentication = jwt.sign(email,"secretuser")
        return res.cookie(authentication).status(200).send("Login berhasil")
    }
    catch(error){
        console.log(error.message)
        return res.send("login gagal")
    }
}
const findThisOrder = async(req, res) => {
    const loginuserid = req.user
    const orderid = req.params.orderid
   
    try{const foundOrder = await Order.findByPk(orderid, {include: Orderdetails})
    if (foundOrder){
        if (foundOrder.userid != loginuserid){
            return res.status(403).send('Maaf, Anda tidak boleh mengakses order ini')
        }
        return res.send(foundOrder)
    }
    if (foundOrder === null){
        return res.status(404).send('Order tidak ditemukan')
    }}
    catch(error){
        console.log(error.message)
        return res.send('terjadi kegagalan')
    }
}
const findUserOrders = async(req, res) => {
    const userid = req.user
    try{
        const foundOrders = await Order.findAll({where:{
            id: userid
        }},{include: Orderdetails})
        if(foundOrders === null){
            return res.send ('Anda belum membuat order')
        }
        return res.send(foundOrders)
    }
    catch(error){
        console.log(error.message)
        return res.send('Maaf, gagal')
    }
}
const userHandler = {
    register, login, findThisOrder, cekToken, findUserOrders
}


module.exports = userHandler;