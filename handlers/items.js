const db = require('../models')
const items = db.sequelize.model('items')
const lihatProduct = async (req, res) =>{
    const itemid = req.params.itemid
    try{ const foundItem = await items.findByPk(itemid)
        if(foundItem === null){
            return res.status(404).send('Maaf, item tidak ditemukan')
        }
        res.send(foundItem)
    }
    catch(error){
        console.log(error.message)
        return res.send('Item tidak dapat ditampilkan')
    }
}
const itemHandler = {lihatProduct}
module.exports = itemHandler