const db = require('../models');
const orderdetails = db.sequelize.model('orderdetails')
const User = db.sequelize.model('users')
const Order = db.sequelize.model('orders');

const cekToken = async (req, res, next) => {
    let authHeader = req.headers[authorization];
    const token = authHeader.split(' ')[1];
    try {
        const email = jwt.verify(token, "secretuser");
        const getUserID = await User.findOne({where:{
            email: email
    }})
    req.user = getUserID.id}
    catch(error){
        console.log(error.message)
        return res.status(401).send('Unauthorized user, access forbidden')
    }
    next()}

const addOrder = async (req, res) => {
    //order id otomatis
    //status default 'belum bayar'
    const {user_id, orderedItems, total} = req.body;
    const orderAdded = await Order.create({user_id:user_id, total: total});
    if (orderAdded) {
        const ORDERID = orderAdded.id
        //gunakan for loop dengan array items_ordered untuk mengupdate order details
        //ambil orderID dari orderAdded.id
        //res order dikonfirmasi oleh sistem
        for(let item in orderedItems){
            const itemID = item.ID
            const harga = item.harga
            const jumlah = item.jumlah
            const subtotal = item.subtotal
            const detailBaru = await orderdetails.create({orderID: ORDERID, itemID: itemID, harga: harga, jumlah: jumlah, subtotal: subtotal})
            await orderAdded.addorderdetails(detailBaru)
        }
        
        res.send("Order masuk ke sistem, order ID Anda adalah " + ORDERID)
    }
}
const Pembayaran = async (req, res) => {
    const orderID = req.params.orderid;
    //req.user dari middleware cekToken
    const userid = req.user
    try{const foundOrder = await Order.findByPk(orderID)
    //bila order tidak ada
    if(foundOrder === null){
        return res.status(404).send(
            "Maaf, order tidak ditemukan"
        )
    }
    //bila user yang login tidak sama dengan user yang memesan
    if(foundOrder.userid != userid){
        return res.status(403).send('Unauthorized user')
    }
    //bila order sudah dibayar
    if(foundOrder.status != 'belum bayar'){
        return res.status(400).send("Order sudah dibayar")
    }
    const totalPaid = await Order.update({status:"lunas, menunggu pengiriman"}, {where:{
            id: orderID
        }})
    return res.send(totalPaid)
    }

    catch(error){
        console.log(error.message)
        return res.send('Maaf, pembayaran gagal')
    }

    
    }
const Pengiriman = async(req, res)=>{
    const orderID = req.params.orderid
    try{
        const foundOrder = await Order.findByPk(orderID)
        if(foundOrder === null) {
            return res.status(404).send("Maaf, order tidak ditemukan")
        }
        if(foundOrder.status != "lunas, menunggu pengiriman"){
            return res.status(400).send("Order belum dibayar atau sudah dikirim")
        }
        const sendOrder = await Order.update({status:"Order dalam pengiriman"},{where:{
            id:orderID
        }})
        return res.send(sendOrder)
    }
    catch(error){
        return res.send("Maaf, order tidak dapat dikirim")
    }
}


const orderHandler = { addOrder, Pembayaran, Pengiriman, cekToken}
module.exports = orderHandler