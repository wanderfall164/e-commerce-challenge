const { application } = require('express')
const itemHandler = require('../handlers/items')
const itemRouter = require('express').Router()

itemRouter.get('/items/:itemid', itemHandler.lihatProduct)
module.exports = itemRouter