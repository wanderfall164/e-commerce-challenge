const userHandler = require('../handlers/users');

const userRouter = require('express').Router();

userRouter.post('/register', userHandler.register)
userRouter.get('/login', userHandler.login)
userRouter.get('/user/orders/:orderid', userHandler.cekToken, userHandler.findThisOrder)
userRouter.get('/user/orders/all', userHandler.cekToken, userHandler.findUserOrders)

module.exports = userRouter;