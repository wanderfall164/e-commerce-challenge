const orderHandler = require('../handlers/orders')
const orderRouter = require('express').Router()

orderRouter.post('/order/new', orderHandler.addOrder)
orderRouter.put('/order/:orderid/bayar', orderHandler.cekToken, orderHandler.Pembayaran)
orderRouter.put('/order/:orderid/kirim', orderHandler.Pengiriman)

module.exports = orderRouter